#!/usr/bin/env python3

import argparse
import logging
import json
import os
import io
import sys

from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import utils as ligolw_utils
from ligo.lw.utils.process import register_to_xmldoc

from ligo.scald import utils

from confluent_kafka import Producer
from time import sleep

from lal import GPSTimeNow, LIGOTimeGPS, GreenwichMeanSiderealTime

class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
    pass

lsctables.use_in(LIGOLWContentHandler)

# define input args
parser = argparse.ArgumentParser()

parser.add_argument("--kafka-server", metavar = "string", default = "gstlal.ldas.cit:9196", help = "Sets the url for the kafka broker.")
parser.add_argument("--tag", metavar = "string", default = "mdc_inj", help = "Kafka group id.")
parser.add_argument("--topic", metavar = "string", default = "mdc_inj_stream", help = "Kafka topic to send output.")
parser.add_argument("--time-offset", type = int, required = True, help = "Time offset that h(t) data has been shifted by.")
parser.add_argument("--inj-file", metavar = "file", required = True, help = "Path to file containing the full injection set.")
parser.add_argument("--past-inj-time", type = int, default = 0 , help = "Skip sending injections when current time has passed them by --past-inj-time seconds.")
parser.add_argument("--verbose", help = "Be verbose.")

args = parser.parse_args()

time_offset = args.time_offset

# set up logger
console = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s | mdc_injection_stream : %(levelname)s : %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)
if args.verbose:
	logging.getLogger('').setLevel(logging.DEBUG)
else:
	logging.getLogger('').setLevel(logging.INFO)

# set up producer
kafka_settings = {
    'bootstrap.servers': args.kafka_server,
    'group.id': args.tag
}
producer = Producer(kafka_settings)

# load injection file and sort sim table
xmldoc = ligolw_utils.load_filename(args.inj_file, contenthandler = LIGOLWContentHandler)
simtable = lsctables.SimInspiralTable.get_table(xmldoc)

simtable.sort(key = lambda row: row.geocent_end_time + 10.**-9. * row.geocent_end_time_ns)

# continuously check whether we have passed the next injection in the list
while simtable:
	# remove and return the oldest/first inj in the table
	thisrow = simtable.pop(0)

	timenow = float(GPSTimeNow())

	# if the injection is already passed, skip it
	if timenow - (thisrow.geocent_end_time + time_offset) >= args.past_inj_time:
		logging.debug("Skipping old injection")
		continue

	# sleep until the next inj time
	time_to_sleep = (thisrow.geocent_end_time + time_offset) - timenow
	if time_to_sleep > 0.:
		logging.debug("Sleeping until next injection...")
		sleep(time_to_sleep)

	# once the injection time is just passed, fix the parameters and
	# send the sim inspiral table to kafka

	# open a new xml doc
	outxml = ligolw.Document()
	outxml.appendChild(ligolw.LIGO_LW())

	# write a sim inspiral table with a single row corresponding to this injection
	output_simtable = lsctables.New(lsctables.SimInspiralTable)
	output_row = thisrow

	# fix gpstimes and RA
	output_row.geocent_end_time = thisrow.geocent_end_time + time_offset
	output_row.h_end_time = thisrow.h_end_time + time_offset
	output_row.l_end_time = thisrow.l_end_time + time_offset
	output_row.v_end_time = thisrow.v_end_time + time_offset

	gmst0 = GreenwichMeanSiderealTime(LIGOTimeGPS(thisrow.geocent_end_time + thisrow.geocent_end_time_ns * 10.**-9.))
	gmst = GreenwichMeanSiderealTime(LIGOTimeGPS(thisrow.geocent_end_time + thisrow.geocent_end_time_ns * 10.**-9. + time_offset))
	dgmst = gmst - gmst0
	output_row.longitude = thisrow.longitude + dgmst

	# append the updated row to the new simtable
	output_simtable.append(output_row)

	# add the simtable to the output document
	outxml.childNodes[-1].appendChild(output_simtable)

	sim_msg = io.BytesIO()
	ligolw_utils.write_fileobj(outxml, sim_msg, gz = False)
	outxml.unlink()

	# output to kafka
	timenow = float(GPSTimeNow())

	producer.produce(topic=args.topic, value=sim_msg.getvalue())
	logging.info(f"Sent inj to Kafka at time {timenow}.")
	producer.poll(0)

producer.flush()
logging.info("Sent all injections. Exiting ...")
