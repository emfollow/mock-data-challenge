# Mock Data Challenge

[[_TOC_]]

## General

This repository contains the details, and links to results and other information for the mock data challenge for O4.

## Injections

[Gaussian Injection Set (HDF5 and XML)](https://git.ligo.org/shaon.ghosh/injection_campaign_studies/-/tree/master/lowlatency-gaussian-distributions/minSNR-4-batch10_50K_rerun)

[O3 Replay Injection Set (HDF5 and XML)](https://git.ligo.org/shaon.ghosh/injection_campaign_studies/-/tree/master/lowlatency-replay-distributions/minSNR-4)

NOTE: The XML files do not have the complete information about the injections. For example you will not find the tidal deformability values in the XML files. We highly recommend users to use the HDF5 format of the files.

### Technical Information

- The [Injection-campaign studies page](https://git.ligo.org/shaon.ghosh/injection_campaign_studies) contains information on how the injections were generated.
   - Also see the [issues page](https://git.ligo.org/shaon.ghosh/injection_campaign_studies/-/issues), in particular the discussion in [issue #2](https://git.ligo.org/shaon.ghosh/injection_campaign_studies/-/issues/2).  
   - **The version of the code that was used for generating the first set of injections is [tagged post-review here](https://git.ligo.org/shaon.ghosh/injection_campaign_studies/-/tree/v0.1).**

## Live Status Pages

In theory, the following four replays should all be in sync:

- [O4 Gaussian MDC live status page](https://ldas-jobs.ligo.caltech.edu/~patrick.brockill/llstatus-K1O4GaussianMDC/index.html)

- [O3 Replay MDC live status page](https://ldas-jobs.ligo.caltech.edu/~patrick.brockill/llstatus-K1O3ReplayMDC/index.html)

- [Upstream O3Replay End-to-End Pipeline Status](https://ldas-jobs.ligo.caltech.edu/~patrick.brockill/llstatus-K1O3E2ERep2/index.html)

- [Downstream O3Replay End-to-End Pipeline Status](https://ldas-jobs.ligo.caltech.edu/~patrick.brockill/llstatus-K1O3E2ERep1/)

See below for a description of what the End-to-End replays are meant to accomplish.

## Channel Information

The channels can be found in the live status pages above.

### Strain Channels

The O3 Replay MDC contains both the unmodified strain data (which should be identical to the strain in the [Downstream O3 End-to-End Replay](https://ldas-jobs.ligo.caltech.edu/~patrick.brockill/llstatus-K1O3E2ERep1/)) as well as the strain+injections (the `_INJ1` channels):

- H1
   - `H1:GDS-CALIB_STRAIN_O3Replay`
   - `H1:GDS-CALIB_STRAIN_INJ1_O3Replay`
- L1
   - `L1:GDS-CALIB_STRAIN_O3Replay`
   - `L1:GDS-CALIB_STRAIN_INJ1_O3Replay`
- V1
   - `V1:Hrec_hoft_16384Hz_O3Replay`
   - `V1:Hrec_hoft_16384Hz_INJ1_O3Replay`

Note that certain strain channels available in the Downstream O3 End-to-End Replay are not available here: namely, the `STRAIN_CLEAN`, `GATED_STRAIN` channels, as well as all of the KAGRA streams.

### State Vectors

The O4 Gaussian MDC has state vectors which are meant to indicate that the data is always ready for analysis: `H1:GDS-CALIB_STATE_VECTOR` and `L1:GDS-CALIB_STATE_VECTOR` (always set to `1023=0xb1111111111`) and `V1:DQ_ANALYSIS_STATE_VECTOR` (always set to `4095=0xb111111111111`).

## Time Offset

The time offset is meant to be the same as that for the End-to-End Replays, and is discussed on the [O3 End-to-End Replay page](https://wiki.ligo.org/Computing/DASWG/O3EndToEndReplay).

In a nutshell, each of the replays listed here is comprised of 40 days of data (3456000 seconds), running from [1262304000, 1265760000). These data sets are shifted by a time offset into the future so that they look as if they are being produced now.

The first pass started at 1301856000 and ran until 1305312000, at which point the second pass started and continued until 1308768000, etc.

The time offset needs to be accounted for when comparing the injected sky position to skymaps produced for MDC events. This is due to the fact that the offset is not an integer number of sidereal days, so the all the injected right ascension values within an MDC cycle need to be shifted by the same amount. This can be done by finding the fraction of a sidereal day that the injections will be offset by, then converting that value to radians/degrees and adding it to the injected right ascension values.

## Accessing the Data

- O4 Gaussian MDC
  - Frame files will appear in the following directories: `/dev/shm/kafka/H1_O4GaussianMDC`, `/dev/shm/kafka/L1_O4GaussianMDC`, `/dev/shm/kafka/V1_O4GaussianMDC`.
  - Frame files will also appear in the shared memory partitions: `R5LHO_Data`, `R5LLO_Data`, `R5_VIRGO_Data`.
- O3 Replay MDC
  - Frame files will appear in the following directories: `/dev/shm/kafka/H1_O3ReplayMDC`, `/dev/shm/kafka/L1_O3ReplayMDC`, `/dev/shm/kafka/V1_O3ReplayMDC`.
  - Frame files will also appear in the shared memory partitions: `R6LHO_Data`, `R6LLO_Data`, `R6_VIRGO_Data`.


## Uploading information to GraceDB
- All events to be uploaded to GraceDB Playground.
- No labels are required for uploading events.
- Upload O3 Replay MDC triggers as regular events for search type.
- If you are using the GAUSSIAN MDC, upload events as `search="MDC"`

## Information on Other Replays

Note that, as of November 2021, there are _four_ replays currently active: the O3 Replay MDC, the O4 Gaussian MDC, the Upstream O3 End-to-End Replay, and the Downstream O3 End-to-End Replay. The first two, the **O3 Replay MDC** and **O4 Gaussian MDC** (described in this document), have largely been coordinated by Shaon, and were put into place at the end of 2021.

The **Upstream O3 End-to-End Replay**, put in place in mid-2021, is an attempt to push raw data at the sites through calibration pipelines (and other pipelines), eventually pushing the _h(t)_ streams to the clusters for analysis. It was felt, however, that a more stable source of _h(t)_ data needed to be provided so that upstream services massaging the data, such as the calibration pipelines, had enough freedom to work. So a **Downstream O3 End-to-End Replay** was also put in place, which completely bypassed the upstream components and just replayed the already-computed strain streams to the analysis nodes.

[O3 End-to-End Replay (a.k.a. "Archival Data Replay")](https://wiki.ligo.org/Computing/DASWG/O3EndToEndReplay)

## MDC status:
| Level | End date | GPS start | GPS end | Gstlal | PyCBC | MBTA | SPIIR | CWB | oLIB | MLy | Superevents | 
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 00 | 12 Jan, 2022 | 1322592000 | 1326048000| :x: | :x:  | :x:  | :x:  | :x:  |  :x:  | :x:  |[link](https://gracedb-playground.ligo.org/search/?query=1322592000+..+1326048000&query_type=S&results_format=S) |
| 01 | 21 Feb, 2022 | 1326048000 | 1329504000 | :heavy_check_mark: | :heavy_check_mark:  | :white_check_mark:  | :x:  | :white_check_mark:  |  :x:  | :x:  | [link](https://gracedb-playground.ligo.org/search/?query=1326048000+..+1329504000&query_type=S&results_format=S) |
| 02 | 02 April, 2022 | 1329504000 | 1332960000 | :heavy_check_mark: | :heavy_check_mark: | :white_check_mark: | :x: | :white_check_mark: | :white_check_mark: |:x: |[link](https://gracedb-playground.ligo.org/search/?query=1329504000+..+1332960000&query_type=S&results_format=S) |
| 03 | 12 May, 2022 | 1329504000 | 1332960000 | :white_check_mark: | :heavy_check_mark: | :white_check_mark: | :heavy_check_mark: | :white_check_mark: | :white_check_mark: |:white_check_mark: |[link](https://gracedb-playground.ligo.org/search/?query=1332960000+..+1336416000&query_type=S&results_format=S) |
| 04 | 21 June, 2022 | 1336416000 | 1339872000 | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |:white_check_mark: |[link](https://gracedb-playground.ligo.org/search/?query=1336416000+..+1339872000&query_type=S&results_format=S) |
| 05 | 31 July, 2022 | 1339872000 | 1343328000 | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |:white_check_mark: |[link](https://gracedb-playground.ligo.org/search/?query=1339872000+..+1343328000&query_type=S&results_format=S) |
| 06 | 09 Sep, 2022 | 1343328000 | 1346784000 | - | - | - | - | - | - | - |[link](https://gracedb-playground.ligo.org/search/?query=1343328000+..+1346784000&query_type=S&results_format=S) |
| 07 | 19 Oct, 2022 | 1346784000 | 1350240000 | - | - | - | - | - | - | - |[link](https://gracedb-playground.ligo.org/search/?query=1346784000+..+1350240000&query_type=S&results_format=S) |
| 08 | 28 Nov, 2022 | 1350240000 | 1353696000 | - | - | - | - | - | - | - |[link](https://gracedb-playground.ligo.org/search/?query=1350240000+..+1353696000&query_type=S&results_format=S) |
| 09 | 07 Jan, 2023 | 1353696000 | 1357152000 | - | - | - | - | - | - | - |[link](https://gracedb-playground.ligo.org/search/?query=1353696000+..+1357152000&query_type=S&results_format=S)|
| 10 | 16 Feb, 2023 | 1357152000 | 1360608000 | - | - | - | - | - | - | - |[link](https://gracedb-playground.ligo.org/search/?query=1357152000+..+1360608000&query_type=S&results_format=S)|

:heavy_check_mark: - Running on the O3 Replay data only

:white_check_mark: - Running on O3 replay data with MDC injection 

:x: - Not participating on this level

## MDC level 04 report:
In this level all the CBC pipeline and three burst pipelines participated for the first time. The recovered triggers seem to be consistent with the injected signals (see the [comparison plot between optimal SNR and recovered SNR here](https://ldas-jobs.ligo.caltech.edu/~shaon.ghosh/O4_studies/MDC/MDC-level-04/far_thresholded_snr_comparison.gif)).



## Review

The review wiki page is cross-referenced [**here**](https://git.ligo.org/emfollow/mock-data-challenge/-/wikis/Review-Home). The review is currently led by Deep Chatterjee.


## Builder's List

Publications resulting from the MDC should (at a minimum) request authorship from: Patrick Brockill, Michael Coughlin, Reed Essick, Shaon Ghosh, Patrick Godwin, and Erik Katsavounidis. Results making use of summary statistics based on MDC results should additionally include Deep Chatterjee, Sushant Chaudhary,  Andrew Toivonen, Sarah Antier and Gaurav Waratkar.

