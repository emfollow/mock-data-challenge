import argparse

"""
SUMMARY:
When the driver script is run on condor over multiple frames using the cache file options,
some jobs will fail. These failures will not be recorded in condor if the failure occurs
internally in the os.system call. Thus, the rescue dag will not be able to relaunch these
jobs. The address this issue this script is written which will match all the newly created 
frame files with the original frame files in the cache and creates an new cache with only
the missing frames so that a new condor job can be resubmitted.

"""

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-cache", action="store",
                    help="Name of the input cache file")
parser.add_argument("-o", "--output-cache", action="store",
                    help="Name of the output cache file")
parser.add_argument("-m", "--missing-cache", action="store",
                    help="Name of the new cache file with missing frame file names")

args = parser.parse_args()


with open(args.input_cache, 'r') as f:
    input_lines = f.readlines()
    input_tag = []
    tag_file_dict = {}
    for line in input_lines:
        inputfile = line.split('\n')[0]
        thistag = inputfile.split('/')[-1].split('filled-')[-1].split('.gwf')[0]
        tag_file_dict[thistag] = inputfile
        input_tag.append(thistag)
    
with open(args.output_cache, 'r') as f:
    output_lines = f.readlines()
    output_tag = []
    for line in output_lines:
        output_tag.append(line.split('\n')[0].split('/')[-1].split('.gwf')[0].split('50KInj-')[-1])

count = 0
missing = 0
with open(args.missing_cache, 'w') as f:
    for tag in input_tag:
        if tag in output_tag:
            count += 1
        else:
            missing += 1
            f.writelines("{}\n".format(tag_file_dict[tag]))
print("Number of missing frames = {}".format(missing))

