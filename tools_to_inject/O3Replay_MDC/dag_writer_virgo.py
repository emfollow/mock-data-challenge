import os
import sys
import uuid
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-N", "--num", action="store", type=int, help="Number of jobs")
parser.add_argument("-S", "--start", action="store", type=int, default=0, help="Starting job number")
parser.add_argument("-F", "--filename", action="store", help="Name of the dag file")
args = parser.parse_args()

N = args.num
with open(args.filename, 'w') as f:
    for ii in range(args.start, N):
        H_job = (uuid.uuid1()).hex
        L_job = (uuid.uuid1()).hex
        dagtext ='''JOB {} inject_in_Vframes.sub
RETRY {} 0
VARS {} microid="{}" macroout="output_dir_V"

'''.format(L_job, L_job, L_job, ii)
        f.writelines(dagtext)
