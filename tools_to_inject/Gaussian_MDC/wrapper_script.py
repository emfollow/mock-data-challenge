import os
import json
import sys
import time
import numpy as np
import argparse

"""
SUMMARY:
This script is used to create frame files of a specific duration and an injection file.
The time stamps of the start times of the frames are obtained from a time file. The 
generate the Gaussian noise a PSD file must be supplied. 

METHOD:
The code uses the pycbc_condition_strain tool of PyCBC to inject the waveforms in the
fake Gaussian strain. The index number helps the code to find the set of start times 
to create the data corresponding to these times. This was done to facilitate running
multiple instances of this code parallely. 

"""


parser = argparse.ArgumentParser()
parser.add_argument("-d", "--duration", type=int, action="store", default=4096,
                    help="duration of the frames")
parser.add_argument("-f", "--time-file", type=str, action='store',
                    help="Name of the file wih the times stamps of the frames")
parser.add_argument("-I", "--injection-file", type=str, action='store',
                    help="Name of injection HDF5 file")
parser.add_argument("-p", "--psdfile", type=str, action='store',
                    help="Name of the PSD file")
parser.add_argument("-D", "--det", type=str, action='store',
                    help="Name of the detector")
parser.add_argument("-i", "--index", type=str, action='store',
                    help="Key of the JSON file")
parser.add_argument("-o", "--outdir", type=str, action='store', default="frames",
                    help="Name of the output directory")
parser.add_argument("-s", "--seed", type=int, action="store", default=100,
                    help="Random seed for pycbc_condition_strain")

args = parser.parse_args()


key = args.index

with open(args.time_file, 'r') as f:
    data = json.load(f)

tstarts = data[key]
print(tstarts)

seg_len = args.duration
injection_file = args.injection_file

x = "{start}"
y= "{duration}"
det = args.det + "-" + args.det+ "1"
ti = time.time()
total_time = 0
count = 1
for t in tstarts:
    thisiter0 = time.time()
    cmd = "pycbc_condition_strain --injection-file {} --channel-name {}1:MDC-STRAIN \
           --output-strain-file {}_STRAIN-{}-{}.gwf --fake-strain-from-file {} \
           --fake-strain-seed {} --low-frequency-cutoff 11 --fake-strain-flow 11 \
           --frame-duration {}  --gps-start-time {} --gps-end-time {} \
           --sample-rate 16384".format(args.injection_file, args.det, det, x, y, args.psdfile, args.seed, seg_len, t, t + seg_len)

    print('Creating frame file for segment {} ---> {}'.format(t, t + seg_len))
    os.system(cmd)
    thisiter1 = time.time()
    total_time += thisiter1 - thisiter0
    mean_run_time = total_time/count
    print('Average time taken in this loop = {}'.format(mean_run_time))
    time_left = np.round(mean_run_time * (len(tstarts) - count)/3600, 2)
    print('\n *** Expected time to finish = {} hours ***\n'.format(time_left))
    count += 1
tf = time.time()

print("Time taken to create {} frames = {} s".format(len(tstarts), tf - ti))


os.system('mkdir -p {}'.format(args.outdir))
os.system("mv {}_STRAIN-*.gwf {}/.".format(det, args.outdir))


