import json
import argparse
import numpy as np


parser = argparse.ArgumentParser()
parser.add_argument("-n", "--numsets", action="store", type=int,
                    default=1, help="Number of time files to be created")
parser.add_argument("-N", "--num", action="store", type=int,
                    help="Number of jobs you want to run")
parser.add_argument("-s", "--start", action="store", type=int,
                    default=1262304000, help="GPS start time of you dataset")
parser.add_argument("-e", "--end", action="store", type=int,
                    default=1265761024, help="GPS end time of you dataset")
parser.add_argument("-d", "--duration", action="store", type=int,
                    default=4096, help="Segment length of frames")
parser.add_argument("-o", "--output", action="store",
                    help="Name of the output JSON file. If numsets > 1, then use this as a tag")

args = parser.parse_args()

# check if proper time boundary and segment duration is provided
numframes = (args.end - args.start)/args.duration
if numframes != int(numframes):
    numframes = np.round(numframes)
    suggested_end = int(args.start + numframes*args.duration)
    suggested_start = int(args.end - numframes*args.duration)
    print("\nGPS times boundary inconsistent with segment duration")
    print("Suggested GPS times:")
    print("{} ---> {}".format(args.start, suggested_end))
    print("---OR---")
    print("{} ---> {}\n".format(suggested_start, args.end))
    assert False, "GPS times boundary inconsistent with segment duration"

tstarts = np.arange(args.start, args.end, args.duration)
chunks = np.array_split(tstarts, args.num)
chunk_list = [chunk.tolist() for chunk in chunks]
IDs = np.arange(args.num, dtype=int).astype(str)
jobs_dict = dict(zip(IDs, chunk_list))

with open(args.output, "w") as f:
    json.dump(jobs_dict, f, indent=2)
