n=$1
output=$2
timefile=$3
injfile=$4
psdfile=$5
detector=$6
seed=$7
mkdir -p outfiles

for ((i = 0; i <= n; i++));
do
	echo "Run number = "$i
	python wrapper_script_git.py -d 4096 -f $timefile  -i $i -I $injfile -p $psdfile -s $seed -D $detector -o $output > outfiles/stdout$i.out &
done
