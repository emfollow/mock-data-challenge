#!/bin/bash

#
# Here is an example of how the entire 40-day stretch of data can be created
# in the current directory:
# ./DMTGen-LLO-condor.sh 1262342144 1265762304 /scratch/patrick.brockill/O3E2E_data/fill/L1 \
#   /scratch/patrick.brockill/shaon/output_dir_L_symlink L-L1_O3ReplayMDC $(pwd)

#
# To just quickly test that this works, just create 10 seconds of data instead:
# ./DMTGen-LLO-condor.sh 1262342144 $((1262342144+10)) /scratch/patrick.brockill/O3E2E_data/fill/L1 \
#   /scratch/patrick.brockill/shaon/output_dir_L_symlink L-L1_O3ReplayMDC $(pwd)

#
# ARG
# 1    Start GPS
# 2    End GPS
# 3    Directory where Patrick's O3 Replay files are kept
# 4    Directory where Shaon's O3 Replay MDC injection files are kept
# 5    The prefix for the .gwf files which will be created
# 6    The directory where the .gwf files will be created

#
# When condorizing this, be aware that you need to set the End GPS time to
# the Start GPS time plus multiples of 4096.

start_time=$1
end_time=$2
o3_dir=$3
inj_dir=$4
file_prefix=$5
output_dir=$6

conf=$(cat <<EOT
Parameter StartGPS	${start_time}
Parameter EndGPS      ${end_time}
Parameter Compression "zero-suppress-or-gzip"
Parameter OutputDirectory ${output_dir}
Parameter FrameLength 4096
Parameter FilePrefix  "${file_prefix}"
Source source0 "FrameData(Channel=L1:DMT-DQ_VECTOR,Files=${o3_dir}/*.gwf)"
Channel L1:DMT-DQ_VECTOR source0
Source source1 "FrameData(Channel=L1:DMT-DQ_VECTOR_GATED,Files=${o3_dir}/*.gwf)"
Channel L1:DMT-DQ_VECTOR_GATED source1
Source source2 "FrameData(Channel=L1:GDS-CALIB_F_CC,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_F_CC source2
Source source3 "FrameData(Channel=L1:GDS-CALIB_F_CC_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_F_CC_NOGATE source3
Source source4 "FrameData(Channel=L1:GDS-CALIB_F_S_SQUARED,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_F_S_SQUARED source4
Source source5 "FrameData(Channel=L1:GDS-CALIB_F_S_SQUARED_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_F_S_SQUARED_NOGATE source5
Source source6 "FrameData(Channel=L1:GDS-CALIB_KAPPA_C,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_C source6
Source source7 "FrameData(Channel=L1:GDS-CALIB_KAPPA_C_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_C_NOGATE source7
Source source8 "FrameData(Channel=L1:GDS-CALIB_KAPPA_PUM_IMAGINARY,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_PUM_IMAGINARY source8
Source source9 "FrameData(Channel=L1:GDS-CALIB_KAPPA_PUM_IMAGINARY_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_PUM_IMAGINARY_NOGATE source9
Source source10 "FrameData(Channel=L1:GDS-CALIB_KAPPA_PUM_REAL,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_PUM_REAL source10
Source source11 "FrameData(Channel=L1:GDS-CALIB_KAPPA_PUM_REAL_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_PUM_REAL_NOGATE source11
Source source12 "FrameData(Channel=L1:GDS-CALIB_KAPPA_TST_IMAGINARY,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_TST_IMAGINARY source12
Source source13 "FrameData(Channel=L1:GDS-CALIB_KAPPA_TST_IMAGINARY_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_TST_IMAGINARY_NOGATE source13
Source source14 "FrameData(Channel=L1:GDS-CALIB_KAPPA_TST_REAL,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_TST_REAL source14
Source source15 "FrameData(Channel=L1:GDS-CALIB_KAPPA_TST_REAL_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_TST_REAL_NOGATE source15
Source source16 "FrameData(Channel=L1:GDS-CALIB_KAPPA_UIM_IMAGINARY,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_UIM_IMAGINARY source16
Source source17 "FrameData(Channel=L1:GDS-CALIB_KAPPA_UIM_IMAGINARY_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_UIM_IMAGINARY_NOGATE source17
Source source18 "FrameData(Channel=L1:GDS-CALIB_KAPPA_UIM_REAL,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_UIM_REAL source18
Source source19 "FrameData(Channel=L1:GDS-CALIB_KAPPA_UIM_REAL_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_KAPPA_UIM_REAL_NOGATE source19
Source source20 "FrameData(Channel=L1:GDS-CALIB_SRC_Q_INVERSE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_SRC_Q_INVERSE source20
Source source21 "FrameData(Channel=L1:GDS-CALIB_SRC_Q_INVERSE_NOGATE,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_SRC_Q_INVERSE_NOGATE source21
Source source22 "FrameData(Channel=L1:GDS-CALIB_STATE_VECTOR,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_STATE_VECTOR source22
Source source23 "FrameData(Channel=L1:GDS-CALIB_STRAIN,Files=${o3_dir}/*.gwf)"
Channel L1:GDS-CALIB_STRAIN source23
# Source source24 "FrameData(Channel=L1:GDS-CALIB_STRAIN_CLEAN,Files=${o3_dir}/*.gwf)"
# Channel L1:GDS-CALIB_STRAIN_CLEAN source24
# Source source25 "FrameData(Channel=L1:GDS-GATED_STRAIN,Files=${o3_dir}/*.gwf)"
# Channel L1:GDS-GATED_STRAIN source25
Source source26 "FrameData(Channel=L1:IDQ-EFF_OVL_16_4096,Files=${o3_dir}/*.gwf)"
Channel L1:IDQ-EFF_OVL_16_4096 source26
Source source27 "FrameData(Channel=L1:IDQ-FAP_OVL_16_4096,Files=${o3_dir}/*.gwf)"
Channel L1:IDQ-FAP_OVL_16_4096 source27
Source source28 "FrameData(Channel=L1:IDQ-LOGLIKE_OVL_16_4096,Files=${o3_dir}/*.gwf)"
Channel L1:IDQ-LOGLIKE_OVL_16_4096 source28
Source source29 "FrameData(Channel=L1:IDQ-OK_OVL_16_4096,Files=${o3_dir}/*.gwf)"
Channel L1:IDQ-OK_OVL_16_4096 source29
Source source30 "FrameData(Channel=L1:IDQ-PGLITCH_OVL_16_4096,Files=${o3_dir}/*.gwf)"
Channel L1:IDQ-PGLITCH_OVL_16_4096 source30
Source source31 "FrameData(Channel=L1:IDQ-RANK_OVL_16_4096,Files=${o3_dir}/*.gwf)"
Channel L1:IDQ-RANK_OVL_16_4096 source31
#
# Shaon's data
Source source32 "FrameData(Channel=L1:GDS-CALIB_STRAIN,Files=${inj_dir}/*.gwf)"
Channel L1:GDS-CALIB_STRAIN_INJ1 source32
EOT
)

DMT_IGNORE_NAN=1 DMTGen -conf <(echo "$conf")

