import os
import sys
import uuid
import argparse

import numpy as np


parser = argparse.ArgumentParser()
parser.add_argument("-D", "--duration", action="store", type=int, default=4096, help="Duration of frames")
parser.add_argument("-S", "--start", action="store", type=int, default=0, help="GPS start time of the whole project")
parser.add_argument("-E", "--end", action="store", type=int, default=0, help="GPS end time of the whole project")
parser.add_argument("-F", "--filename", action="store", help="Name of the dag file")
parser.add_argument("-O", "--output", action="store", help="Name of of the output frames directory")
args = parser.parse_args()


starts = np.arange(args.start, args.end, args.duration)

os.system("mkdir -p log")
os.system("mkdir -p errors")
os.system("mkdir -p outputs")
os.system("mkdir -p {}".format(args.output))



with open(args.filename, 'w') as f:
    for ii, gps_start in enumerate(starts):
        gps_end = gps_start + args.duration
        H_job = (uuid.uuid1()).hex
        L_job = (uuid.uuid1()).hex
        dagtext ='''# Frame number {}
JOB {} run_DMTGen_LLO.sub
RETRY {} 0
VARS {} macrostart="{}" macroend="{}"
    
JOB {} run_DMTGen_LHO.sub
RETRY {} 0
VARS {} macrostart="{}" macroend="{}"\n
'''.format(ii+1, L_job, L_job, L_job, gps_start, gps_start + args.duration, H_job, H_job, H_job, gps_start, gps_start + args.duration)
        f.writelines(dagtext)
