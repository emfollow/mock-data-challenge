#!/bin/bash

#
# Here is an example of how the entire 40-day stretch of data can be created
# in the current directory:
# ./DMTGen-VIRGO-condor.sh 1262342144 1265762304 /scratch/patrick.brockill/O3E2E_data/fill/V1 \
#   /home/shaon.ghosh/ANALYSIS/O4/analysis/output_dir_V V-V1_O3ReplayMDC $(pwd)

#
# To just quickly test that this works, just create 10 seconds of data instead:
# ./DMTGen-VIRGO-condor.sh 1262304000 $((1262304000+10)) /scratch/patrick.brockill/O3E2E_data/fill/V1 \
#   /home/shaon.ghosh/ANALYSIS/O4/analysis/output_dir_V V-V1_O3ReplayMDC $(pwd)

#
# ARG
# 1    Start GPS
# 2    End GPS
# 3    Directory where Patrick's O3 Replay files are kept
# 4    Directory where Shaon's O3 Replay MDC injection files are kept
# 5    The prefix for the .gwf files which will be created
# 6    The directory where the .gwf files will be created

#
# When condorizing this, be aware that you need to set the End GPS time to
# the Start GPS time plus multiples of 2000.

start_time=$1
end_time=$2
o3_dir=$3
inj_dir=$4
file_prefix=$5
output_dir=$6

conf=$(cat <<EOT
Parameter StartGPS	${start_time}
Parameter EndGPS      ${end_time}
Parameter Compression "zero-suppress-or-gzip"
Parameter OutputDirectory ${output_dir}
Parameter FrameLength 2000
Parameter FilePrefix  "${file_prefix}"
Source source0 "FrameData(Channel=V1:DQ_ANALYSIS_STATE_VECTOR,Files=${o3_dir}/*.gwf)"
Channel V1:DQ_ANALYSIS_STATE_VECTOR source0
Source source1 "FrameData(Channel=V1:DQ_ANALYSIS_STATE_VECTOR_ORIG,Files=${o3_dir}/*.gwf)"
Channel V1:DQ_ANALYSIS_STATE_VECTOR_ORIG source1
# Source source2 "FrameData(Channel=V1:DQ_SEGONLINE_STATE_VECTOR,Files=${o3_dir}/*.gwf)"
# Channel V1:DQ_SEGONLINE_STATE_VECTOR source2
Source source3 "FrameData(Channel=V1:DQ_VETO_CWB,Files=${o3_dir}/*.gwf)"
Channel V1:DQ_VETO_CWB source3
Source source4 "FrameData(Channel=V1:DQ_VETO_GSTLAL,Files=${o3_dir}/*.gwf)"
Channel V1:DQ_VETO_GSTLAL source4
Source source5 "FrameData(Channel=V1:DQ_VETO_MBTA,Files=${o3_dir}/*.gwf)"
Channel V1:DQ_VETO_MBTA source5
Source source6 "FrameData(Channel=V1:DQ_VETO_PYCBC,Files=${o3_dir}/*.gwf)"
Channel V1:DQ_VETO_PYCBC source6
Source source7 "FrameData(Channel=V1:Hrec_STATE_VECTOR,Files=${o3_dir}/*.gwf)"
Channel V1:Hrec_STATE_VECTOR source7
Source source8 "FrameData(Channel=V1:Hrec_hoft_16384Hz,Files=${o3_dir}/*.gwf)"
Channel V1:Hrec_hoft_16384Hz source8
# Source source9 "FrameData(Channel=V1:Hrec_hoft_16384Hz_Gate,Files=${o3_dir}/*.gwf)"
# Channel V1:Hrec_hoft_16384Hz_Gate source9
# Source source10 "FrameData(Channel=V1:Hrec_hoft_16384Hz_GateFlag,Files=${o3_dir}/*.gwf)"
# Channel V1:Hrec_hoft_16384Hz_GateFlag source10
Source source11 "FrameData(Channel=V1:Hrec_hoft_16384Hz_GateMinRange,Files=${o3_dir}/*.gwf)"
Channel V1:Hrec_hoft_16384Hz_GateMinRange source11
Source source12 "FrameData(Channel=V1:Hrec_hoft_16384Hz_GateRange,Files=${o3_dir}/*.gwf)"
Channel V1:Hrec_hoft_16384Hz_GateRange source12
# Source source13 "FrameData(Channel=V1:Hrec_hoft_16384Hz_Gated,Files=${o3_dir}/*.gwf)"
# Channel V1:Hrec_hoft_16384Hz_Gated source13
#
# Shaon's data
Source source14 "FrameData(Channel=V1:Hrec_hoft_16384Hz,Files=${inj_dir}/*.gwf)"
Channel V1:Hrec_hoft_16384Hz_INJ1 source14
EOT
)

DMT_IGNORE_NAN=1 DMTGen -conf <(echo "$conf")

